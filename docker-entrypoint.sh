#!/bin/bash

set -e

log(){
    echo -e "[$(date +'%D %H:%M:%S %Z')] - $*"
}

# Add as command if needed
if [ "${1:0:1}" = '-' ]; then
    set -- bot "$@"
fi

install_gcloud_sdk(){
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
    export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    apt-get update && apt-get -y install google-cloud-sdk
}

if [ "$1" = 'bot' ]; then
  # export USER_ID=$(id -u)
  # export GROUP_ID=$(id -g)
  # envsubst < /passwd.template > /tmp/passwd
  # export LD_PRELOAD=/usr/lib64/libnss_wrapper.so
  # export NSS_WRAPPER_PASSWD=/tmp/passwd
  # export NSS_WRAPPER_GROUP=/etc/group

  # echo $(id)

  install_gcloud_sdk
  python ./bot_notification.py
fi

exec "$@"
