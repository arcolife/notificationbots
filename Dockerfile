FROM ubuntu:18.04
MAINTAINER Archit Sharma <arcshams@gmail.com>

RUN apt-get update && \
    apt-get --assume-yes install build-essential gcc g++ make \
    binutils software-properties-common unzip wget

# Build
COPY src/ /app
# COPY requirements.txt /app/requirements.txt
WORKDIR /app

RUN apt-get --assume-yes install python3-pip python3-dev
RUN pip3 install --no-cache-dir -r requirements.txt

# # Ports
# EXPOSE 8888

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["bot"]
