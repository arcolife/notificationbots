#!/usr/bin/env python3
import re
import delegator
import googleapiclient.discovery
# from oauth2client.client import GoogleCredentials
# credentials = GoogleCredentials.get_application_default()


def list_instances(project=None, wait=False):
    """
    if wait == False:
        run through commandline which is faster
    """
    # gcloud compute instances list
    if not wait:
        c = delegator.run("gcloud compute instances list")
        status = c.out.splitlines()
        headers = status.pop(0).split()
        status = [re.split(r'\s\s+', line) for line in status]
        return status
    else:
        compute = googleapiclient.discovery.build('compute', 'v1')
        zones = compute.zones().list(project=project).execute()
        all_vms = []
        for zone in zones['items']:
            instances = compute.instances()
            instances = instances.list(project=project, zone=zone['name'])
            instances = instances.execute()
            if instances.has_key('items'):
                for item in instances['items']:
                    try:
                        pub_ip = item['networkInterfaces'][0]['accessConfigs'][0]['natIP']
                    except KeyError or IndexError:
                        pub_ip = ''
                    machine_type = item['machineType'].rsplit('/')[-1]
                    private_ip = item['networkInterfaces'][0]['networkIP']
                    all_vms.append([item['name'], zone['name'], machine_type, private_ip, pub_ip, item['status']])
        return all_vms

        # return result['items'] if 'items' in result else None
        # operation = create_instance(compute, project, zone, instance_name, bucket)
        # wait_for_operation(compute, project, zone, operation['name'])
