import os
import logging
# from google.cloud import storage, \
#                          datastore, \
#                          pubsub

############################################################################

# buckets
os.environ['BUCKET_NAME'] = 'startupdl'

# os.environ['PROJECT_ID'] = 'api-project-583233161416'
os.environ['PROJECT_ID'] = 'api-project-583233161416'
os.environ['CLOUD_STORAGE_BUCKET'] = 'rayd8-uploaded-images'

os.environ['AWS_ACCESS_KEY_ID'] = ""
os.environ['AWS_SECRET_ACCESS_KEY'] = ""

# TODO: configure encrypted credentials for container in cloud
os.environ['GOOGLE_OAUTH2_CLIENT_SECRET'] = ''

# Logfile Name
os.environ['LOG_FILE_NAME'] = 'output.log'
os.environ['TEST_LOG_FILE'] = 'test.log'

# Telegram
os.environ['TELEGRAM_KEY'] = '535048361:AAGFYHPMA87cKzIXb5LecMSbIK02aVfKYrg'
os.environ['CHAT_ID'] = "-263773791"

############################################################################

DEBUG = False
# DEBUG = True


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    filename=os.environ['LOG_FILE_NAME'],
                    filemode='a')

logger = logging.getLogger(__name__)

# client = storage.Client(os.environ['PROJECT_ID'])
# cbucket = client.get_bucket(os.environ['CLOUD_STORAGE_BUCKET'])
#
# # GCP's DynamoDB
# ds = datastore.Client(os.environ['PROJECT_ID'])
#
# # GCP's SQS
# subscription_name = 'projects/{project_id}/subscriptions/{sub}'.format(
#     project_id='api-project-583233161416',
#     sub='uploaded_faucet'  # Set this to something appropriate.)
# )
# subscriber_client = pubsub.SubscriberClient()
#
# pub_topic = 'projects/{project_id}/topics/{topic_id}'.format(
#     project_id=os.environ['PROJECT_ID'],
#     topic_id=os.environ['PUB_TOPIC_NAME']
# )
# publisher_client = pubsub.PublisherClient()
