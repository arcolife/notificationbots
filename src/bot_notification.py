#!/usr/bin/env python3

import os
import json
import argparse

from telegram.ext import Updater, CommandHandler  # , MessageHandler, Filters

from aws.dynamodb_check import scan_table_allpages
from aws.ec2_check import check_ec2
from aws.sqs_check import check_sqs

from gcp.datastore_check import scan_entities

from config import logging



def ec2(bot, update):
    raw_json = check_ec2()
    update.message.reply_text(json.dumps(raw_json, indent=1))


def status(bot, update):
    data_dict, user_dict = scan_table_allpages('active_image')
    update.message.reply_text(json.dumps(data_dict, indent=1))
    update.message.reply_text(json.dumps(user_dict, indent=1))


def sqs(bot, update):
    msq = check_sqs()
    update.message.reply_text(json.dumps(msq, indent=1))


def pub(bot, update):
    pass


def gcp(bot, update):
    """
    vm status in gcp
    """
    pass


def k8s(bot, update):
    """
    cluster status in GCP
    """
    pass


def add_base_handlers(dp, func_opts):
    for opt in func_opts:
        dp.add_handler(CommandHandler(opt, eval(opt)))


def help(bot, update):
    if os.environ['INFRA_TYPE'] == 'aws':
        opts = "['/status', '/ec2', '/sqs']"

    update.message.reply_text("Hello I'm Doby the House Elf, commands: ")


if __name__ == '__main__':
    updater = Updater(os.environ['TELEGRAM_KEY'])
    dp = updater.dispatcher

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('infra', metavar='t', type=str,  # nargs='+',
                        help='type of infrastructure [aws/gcp]')
    # parser.add_argument('--sum', dest='accumulate', action='store_const',
    #                     const=sum, default=max,
    #                     help='sum the integers (default: find the max)')
    args = parser.parse_args()
    if args.infra == 'aws':
        os.environ['INFRA_TYPE'] = 'aws'
        func_opts = ['status', 'ec2', 'sqs']
    else:
        os.environ['INFRA_TYPE'] = 'gcp'
        func_opts = ["status", "vms", "k8s", "pub"]

    add_base_handlers(func_opts)
    dp.add_handler(CommandHandler("help", help))
    updater.start_polling()
