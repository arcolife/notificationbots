#!/usr/bin/env python3

from sessions import boto_session
from collections import defaultdict

dynamodb_resource = boto_session.resource('dynamodb')


def scan_table_allpages(table_name):
    table = dynamodb_resource.Table(table_name)
    data_dict = {'Ready': 0,
                 'Processing': 0}

    user_dict = defaultdict(int)

    response = table.scan()

    for i in response['Items']:
        data_dict[i['status']] += 1
        name = i['filename'].split('_')[1]
        user_dict[name] += 1

    return data_dict, user_dict


if __name__ == '__main__':
    print(scan_table_allpages('active_image'))
